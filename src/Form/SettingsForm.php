<?php

namespace Drupal\countly\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Countly settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'countly_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['countly.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Countly URL'),
      '#description' => $this->t('Countly URL server'),
      '#default_value' => $this->config('countly.settings')->get('url'),
    ];
    $form['app_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Countly App Key'),
      '#description' => $this->t('Countly App Key'),
      '#size' => 40,
      '#maxlength' => 40,
      '#default_value' => $this->config('countly.settings')->get('app_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('app_key')) != 40) {
      $form_state->setErrorByName('app_key', $this->t('Incorrect App Key.'));
    }

    if (!preg_match('/^https?:\/\//', $form_state->getValue('url'))) {
      $form_state->setErrorByName('url', $this->t('Invalid URL.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('countly.settings')
      ->set('url', $form_state->getValue('url'))
      ->set('app_key', $form_state->getValue('app_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
